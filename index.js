function BookModel(library, id, title) {
	this.library = library;
	this.id = id;
	this.title = title;
}

BookModel.prototype.updateBook = async function (book) {
	var library = book.library;
	delete book.library;
	await fetch("http://localhost:3000/v1/books/" + book.id, {
		method: 'PUT',
		body: JSON.stringify(book)
	});
	book.library = library;
	this.library.fetchBooks();
}

function LibraryModel() {
	this.books = []
	this.booksChanged = () => { }
	this.fetchBooks();
}

LibraryModel.prototype.fetchBooks = function () {
	// TODO: await?
	fetch('http://localhost:3000/v1/books')
		.then((response) => response.json())
		.then((data) => {
			this.books = []
			for (let book of data)
				this.books.push(new BookModel(this, book.id, book.title));
			this.booksChanged();
		});
}

LibraryModel.prototype.addBook = async function (title) {
	await fetch("http://localhost:3000/v1/books", {
		method: 'POST',
		body: JSON.stringify({ title: title })
	});
	this.fetchBooks();
}

LibraryModel.prototype.deleteBook = async function (id) {
	await fetch("http://localhost:3000/v1/books/" + id, {
		method: 'DELETE'
	});
	this.fetchBooks();
}

function BookView(model) {
	this.model = model;
}

BookView.prototype.handleEdit = function () {
	this.onEdit(this.model);
}

BookView.prototype.handleDelete = function () {
	this.model.library.deleteBook(this.model.id);
}

BookView.prototype.render = function () {
	var el = document.createElement('li');
	el.innerText = this.model.title;

	var editButton = document.createElement("button");
	editButton.innerHTML = "Edit";
	editButton.addEventListener("click", this.handleEdit.bind(this));
	el.appendChild(editButton);

	var deleteButton = document.createElement("button");
	deleteButton.innerHTML = "Delete";
	deleteButton.addEventListener("click", this.handleDelete.bind(this));
	el.appendChild(deleteButton);

	return el;
}

function BookEditView(model) {
	this.model = model;
}

BookEditView.prototype.handleSave = function () {
	this.model.title = this.titleEdit.value;
	this.model.updateBook(this.model);
	this.onEditFinished();
}

BookEditView.prototype.render = function () {
	var libraryEl = document.getElementById('library');
	libraryEl.innerHTML = "";

	this.titleEdit = document.createElement("input");
	this.titleEdit.value = this.model.title;
	libraryEl.appendChild(this.titleEdit);

	var saveButton = document.createElement("button");
	saveButton.innerText = "Save"
	saveButton.addEventListener("click", () => this.handleSave());
	libraryEl.appendChild(saveButton);

	var cancelButton = document.createElement("button");
	cancelButton.innerText = "Cancel"
	cancelButton.addEventListener("click", this.onEditFinished);
	libraryEl.appendChild(cancelButton);
}

function BookListView(model) {
	this.model = model;
}

BookListView.prototype.handleAdd = function () {
	this.model.addBook(this.titleEdit.value);
	this.titleEdit.value = '';
	this.titleEdit.focus();
}

BookListView.prototype.handleEdit = function (bookModel) {
	var bookEditView = new BookEditView(bookModel);
	bookEditView.onEditFinished = this.render.bind(this);
	bookEditView.render();
}

BookListView.prototype.updateBooksList = function () {
	this.booksEl.innerHTML = '';
	for (let book of this.model.books) {
		let bookView = new BookView(book);
		bookView.onEdit = this.handleEdit.bind(this);
		this.booksEl.appendChild(bookView.render());
	}
}

// TODO: VS Code `Format Document` додає пропуск перед `()` для анонімних функцій. ОК?
BookListView.prototype.render = function () {
	var libraryEl = document.getElementById('library');
	libraryEl.innerHTML = "";

	var booksListLabel = document.createElement("p");
	booksListLabel.innerText = "Books:"
	libraryEl.appendChild(booksListLabel);

	this.booksEl = document.createElement("ul");
	libraryEl.appendChild(this.booksEl);

	this.titleEdit = document.createElement("input");
	libraryEl.appendChild(this.titleEdit);

	var addButton = document.createElement("button");
	addButton.innerText = "Add"
	addButton.addEventListener("click", () => this.handleAdd());
	libraryEl.appendChild(addButton);

	this.updateBooksList();
}

function LibraryView(model) {
	this.model = model;
}

LibraryView.prototype.render = function () {
	var libraryEl = document.getElementById('library');
	libraryEl.innerHTML = "";

	document.getElementsByTagName("h1")[0].addEventListener("click",
		() => this.render());

	var booksLink = document.createElement("a");
	booksLink.innerHTML = "Books"
	booksLink.addEventListener("click", () => (new BookListView(this.model)).render())
	libraryEl.appendChild(booksLink);

	var authorsLink = document.createElement("a");
	authorsLink.innerHTML = "Authors"
	libraryEl.appendChild(authorsLink);
}

// TODO: Який правильний конвеншин: анонімні функції чи arrow-функції?
document.addEventListener("DOMContentLoaded", function () {
	var model = new LibraryModel();
	var view = new LibraryView(model);
	view.render();
	//model.booksChanged = () => view.updateBooksList();
	//model.booksChanged = view.updateBooksList.bind(view);
});
